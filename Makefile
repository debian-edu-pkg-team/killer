SCRIPTNAME = killer

# Man page information
SECTION = 1
RELEASE = " "
CENTER = " "

TARGETS = $(SCRIPTNAME).html $(SCRIPTNAME).$(SECTION) $(SCRIPTNAME).txt

all:	$(TARGETS)

$(SCRIPTNAME).html: $(SCRIPTNAME)
	pod2html $(SCRIPTNAME) --outfile=$@

$(SCRIPTNAME).$(SECTION): $(SCRIPTNAME)
	pod2man --section=$(SECTION) --release=$(RELEASE) --center=$(CENTER) $(SCRIPTNAME) > $@

$(SCRIPTNAME).txt: $(SCRIPTNAME)
	pod2text $(SCRIPTNAME) > $(SCRIPTNAME).txt

clean:
	-rm -f $(TARGETS) pod2html-*cache

install:
	@echo "You probably want to customize the script then copy it into place yourself." 1>&2
